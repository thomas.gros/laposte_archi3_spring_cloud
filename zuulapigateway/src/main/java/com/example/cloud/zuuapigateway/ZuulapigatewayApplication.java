package com.example.cloud.zuuapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class ZuulapigatewayApplication {

    @Bean
    public LogFilter logFilter() {
        return new LogFilter();
    }

    public static void main(String[] args) {
        SpringApplication.run(ZuulapigatewayApplication.class, args);
    }

}
