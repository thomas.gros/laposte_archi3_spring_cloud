package com.example.cloud.functionweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.function.Supplier;

@SpringBootApplication
@EnableBinding(Source.class)
public class SourceFromSupplier {

    // enable spring-cloud-stream et spring-cloud-starter-stream-rabbit

    public static void main(String[] args) {
        SpringApplication.run(SourceFromSupplier.class, "--spring.cloud.stream.function.definition=date");
    }
    @Bean
    public Supplier<Date> date() {
        System.out.println("send date");
        return () -> new Date(12345L);
    }
}