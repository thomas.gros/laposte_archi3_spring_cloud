package com.example.cloud.cloudstreamhello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableBinding(Sink.class)
public class CloudstreamHelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudstreamHelloApplication.class, args);
    }

    public static class Data {
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Data [value=" + value + "]";
        }

    }

    @StreamListener(Sink.INPUT)
    public void handle(Data data) {
        System.out.println(data);
    }


}
