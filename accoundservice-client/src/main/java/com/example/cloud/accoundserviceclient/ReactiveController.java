package com.example.cloud.accoundserviceclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class ReactiveController {

    @Bean
    @LoadBalanced
    private WebClient.Builder webClientBuilder() {
        return WebClient.builder();
    }

    @Autowired
    private WebClient.Builder webClientBuilder;


    @GetMapping("/consume-helloworld-hystrix")
    @ResponseBody
    public Mono<String> consumeHelloWorld() {
        return HystrixCommands
                .from(helloCommand())
                .fallback(fallbackCommand())
                .commandName("helloworld-command")
                .toMono();
    };

    public Mono<String> helloCommand() {
        return webClientBuilder
                .baseUrl("lb://accountservice")
                .build()
                .get()
                .uri("/helloworld")
                .retrieve()
                .bodyToMono(String.class);
    }

    public Mono<String> fallbackCommand() {
        return Mono.just("fallback");
    }


    @GetMapping(path="/accounts-names",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<String> getAccountNames() {
        return webClientBuilder
                .baseUrl("lb://accountservice")
                .build()
                .get()
                .uri("/accounts-sse")
                .exchange()
                .flatMapMany(cr -> cr.bodyToFlux(Account.class))
                .map(a -> a.getUsername());
    }

    @GetMapping(path="/accounts-names-rest",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Flux<Account> getAccountNamesrest() {
        return webClientBuilder
                .baseUrl("lb://accountservice")
                .build()
                .get()
                .uri("/accounts-sse")
                .exchange()
                .flatMapMany(cr -> cr.bodyToFlux(Account.class))
                .map(a -> a);
    }
}
