package com.aiconoa.trainings.spring.cloud;

import java.util.function.Function;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import reactor.core.publisher.Flux;

@SpringBootApplication
public class CloudFunctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudFunctionApplication.class, args);
	}
	
	@Bean
	public Function<Flux<String>, Flux<String>> uppercase() {
		return flux -> flux.map(v -> v.toUpperCase());
	}
	
//	@Bean
//	public CommandLineRunner cli() {
//		return args -> {
//			uppercase()
//				.apply(Flux.just("1","2","3"))
//				.map(t -> t + "a")
//				.subscribe(System.out::println);
//		};
//	}
	
}
