package com.example.cloud.cloudstreamtransform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;

@SpringBootApplication
@EnableBinding(Processor.class)
public class CloudstreamTransformApplication {

    private static Logger LOGGER
            = LoggerFactory.getLogger(CloudstreamTransformApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(CloudstreamTransformApplication.class, args);
    }

    public static class Data {
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Data [value=" + value + "]";
        }
    }

    @Transformer(
            inputChannel = Processor.INPUT,
            outputChannel = Processor.OUTPUT)
    public Data transform(String raw) {
        LOGGER.info("transformer received raw message " + raw);
        Data d = new Data();
        d.setValue(raw);
        return d;
    }
}
