package com.example.cloud.cloudstreamsource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.support.MessageBuilder;

import java.util.Random;

@SpringBootApplication
@EnableBinding(Source.class)
public class CloudstreamSourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudstreamSourceApplication.class, args);
    }

    @Autowired
    private Source source;

    @Bean
    public CommandLineRunner sourceCLR() {
       return args -> {
           Random random = new Random();
           for(int i = 0; i < 1000; i++) {
               source.output().send(
                       MessageBuilder
                               .withPayload("raw - " + random.nextInt(1000000))
                               .build());
           }
       } ;
    }
}
