package com.example.cloud.accountservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.Random;

//@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class AccountserviceApplication {

	@Autowired
	private MongoRepo mongoRepo;

	@Autowired
	private Environment env;
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountserviceApplication.class);
	@Bean
	public CommandLineRunner preLoadMongo() throws Exception {
		return args-> {

			LOGGER.info(env.toString());

//            Flux.range(0,100)
//                    .map(i -> new Account(null, "name " + i, 12.3))
//                    .flatMap(mongoRepo::save)
//                    .subscribe(System.out::println);

			mongoRepo
					.saveAll(
							Flux.just(new Account(null, "John", 12.3),
									new Account(null, "Bob", 14.3),
									new Account(null, "Sally", 10.3)))
					.thenMany(mongoRepo.findAll().map(a -> a.toString()))
					.subscribe(System.out::println);


		};
	}

	public static void main(String[] args) {
		SpringApplication.run(AccountserviceApplication.class, args);
	}

}
