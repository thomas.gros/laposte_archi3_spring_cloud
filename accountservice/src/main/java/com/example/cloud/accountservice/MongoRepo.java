package com.example.cloud.accountservice;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

// CrudReactiveRepository
public interface MongoRepo extends ReactiveMongoRepository<Account, String> {
}
